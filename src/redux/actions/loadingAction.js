export const SET_LOADING = 'SET_LOADING'

export const setLoading = (set) => {
    return {
        type: SET_LOADING,
        payload: {
            loading: set
        }
    }
}