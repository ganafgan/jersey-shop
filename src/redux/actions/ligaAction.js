import { setLoading } from "./loadingAction"
import database from '@react-native-firebase/database';
import { dispatchError, dispatchLoading, dispatchSuccess } from "../../utils/Dispatch";
import { showError } from "../../utils/showMessage";

export const GET_LIST_LIGA = 'GET_LIST_LIGA'
export const GET_DETAIL_LIGA = 'GET_DETAIL_LIGA'

export const getListLiga = () => {
    return (dispatch) => {
        dispatchLoading(dispatch, GET_LIST_LIGA)
        database()
        .ref('/ligas')
        .once('value')
        .then((res) => {
            dispatchSuccess(dispatch, GET_LIST_LIGA, res.val())
        })
        .catch((error) => {
            dispatchError(dispatch, GET_LIST_LIGA, error.message)
            showError(error.message)
        })
    }
}

export const getDetailLiga = (id) => {
    return (dispatch) => {
        dispatchLoading(dispatch, GET_DETAIL_LIGA)
        database()
        .ref(`/ligas/${id}`)
        .once('value')
        .then((res) => {
            dispatchSuccess(dispatch, GET_DETAIL_LIGA, res.val())
        })
        .catch((error) => {
            dispatchError(dispatch, GET_DETAIL_LIGA, error.message)
            showError(error.message)
        })
    }
}