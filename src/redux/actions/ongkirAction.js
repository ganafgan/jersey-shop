import axios from "axios"
import { Alert } from "react-native"
import { API_HEADER_RAJAONGKIR, API_KEY, API_RAJAONGKIR, API_TIMEOUT } from "../../utils/Constant"
import { dispatchError, dispatchLoading, dispatchSuccess } from "../../utils/Dispatch"
import { showError } from "../../utils/showMessage"

export const GET_PROVINSI = 'GET_PROVINSI'
export const GET_KOTA = 'GET_KOTA'

export const getProvinsiList = () => {
    return (disptach) => {

        // Loading
        dispatchLoading(disptach, GET_PROVINSI)
        
        axios({
            method: 'GET',
            url: `${API_RAJAONGKIR}province`,
            timeout: API_TIMEOUT,
            headers: API_HEADER_RAJAONGKIR
        })
        .then((res) => {
            if(res.status !== 200) {
                //error
                dispatchError(disptach, GET_PROVINSI, res)
            } else {
                //success
                disptach({
                    type: GET_PROVINSI,
                    payload: {
                        loading: false,
                        data: res.data ? res.data.rajaongkir.results : [],
                        errorMessage: res.data.rajaongkir.status.code
                    }
                })
                dispatchSuccess(disptach, GET_PROVINSI, res.data ? res.data.rajaongkir.results : [])
            }
        })
        .catch((error) => {
            //error
            dispatchError(disptach, GET_PROVINSI, error.message)
            showError(error.message)
        })
    }
}

export const getKotaList = (provinsi_id) => {
    return (disptach) => {

        // Loading
        dispatchLoading(disptach, GET_KOTA)

        axios({
            method: 'GET',
            url: `${API_RAJAONGKIR}city?province=${provinsi_id}`,
            timeout: API_TIMEOUT,
            headers: API_HEADER_RAJAONGKIR
        })
        .then((res) => {
            if(res.status !== 200) {
                //error
                dispatchError(disptach, GET_KOTA, res)
            } else {
                //success
                dispatchSuccess(disptach, GET_KOTA, res.data ? res.data.rajaongkir.results : [])
            }
        })
        .catch((error) => {
            //error
            disptach({
                type: GET_KOTA,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: error
                }
            })
            dispatchError(disptach, GET_KOTA, error.message)
            showError(error.message)
        })
    }
}