import { setLoading } from "./loadingAction"
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database'
import { storeData } from "../../utils/localStorage";
import { showError, showSuccess } from "../../utils/showMessage";
import { dispatchError, dispatchSuccess } from "../../utils/Dispatch";

export const UPDATE_PROFILE = 'UPDATE_PROFILE'
export const UPDATE_AVATAR = 'UPDATE_AVATAR'

export const updateProfile = (data, uid, navigation) => {
    return (dispatch) => {
       
        dispatch(setLoading(true))
        database()
        .ref(`/users/${uid}`)
        .update({
            nama: data.nama,
            alamat: data.alamat,
            nohp: data.nohp,
            provinsi: data.provinsi,
            kota: data.kota,
        })
        .then(() => {
            database().ref(`/users/${uid}`)
            .once('value')
            .then((response) => {
                dispatchSuccess(dispatch, UPDATE_PROFILE, response)
                storeData('userData', response)
            })
            .catch((error) => {
                dispatchError(dispatch, UPDATE_PROFILE, error.message)
                showError(error.response)
            })
            dispatch(setLoading(false))
            navigation.replace('MainApp')
            showSuccess('Update profile success')
        })
        .catch((err) => {
            //error
            dispatch(setLoading(false))
            dispatchError(dispatch, UPDATE_PROFILE, err.message)
            showError(err.message)
        })
    }
}

export const updateAvatar = (photo, uid) => {
    return (dispatch) => {
        database()
        .ref(`/users/${uid}`)
        .update({
            photo_profile: photo
        })
        .then(() => {
            database().ref(`/users/${uid}`)
            .once('value')
            .then((response) => {
                dispatchSuccess(dispatch, UPDATE_AVATAR, response)
                storeData('userData', response)
            })
            .catch((error) => {
                //error
                dispatchError(dispatch, UPDATE_AVATAR, error.message)
                showError(error.message)
            })

        })
        .catch((err) => {
            //error
            dispatchError(dispatch, UPDATE_AVATAR, err.message)
            showError(err.message)
        })
    }
}