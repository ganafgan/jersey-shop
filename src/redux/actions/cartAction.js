import { setLoading } from "./loadingAction"
import database from '@react-native-firebase/database';
import { dispatchError, dispatchLoading, dispatchSuccess } from "../../utils/Dispatch";
import { showError } from "../../utils/showMessage";

export const ADD_TO_CART = 'ADD_TO_CART'

export const addToCart = (data) => {
    return (dispatch) => {
        dispatchLoading(dispatch, ADD_TO_CART)

        //cek apakah data keranjang user sudah ada atau tidak
        database()
        .ref(`/carts/${data.uid}`)
        .once('value')
        .then((res) => {
            if (res.val()) {
                //klo ada update keranjang utama

            } else {
                //simpan keranjang utama
                const carts = {
                    user: data.uid,
                    date: new Date().toDateString(),
                    total: parseInt(data.total) * parseInt(data.jersey.harga),
                    weight: parseInt(data.total) * parseInt(data.jersey.berat)
                }

                database()
                .ref(`/carts/`)
            }
        })
        .catch((error) => {
            dispatchError(dispatch, ADD_TO_CART, error)
            showError(error.message)
        })
    }
}