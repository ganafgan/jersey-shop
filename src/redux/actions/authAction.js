import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { dispatchError, dispatchSuccess } from '../../utils/Dispatch';
import { storeData } from '../../utils/localStorage';
import { showError } from '../../utils/showMessage';
import { setLoading } from './loadingAction';

export const REGISTER_USER = 'REGISTER_USER'
export const LOGIN_USER = 'LOGIN_USER'

export const registerUser = (data, password, navigation) => {
    return (dispatch) => {

        dispatch(setLoading(true))

        auth()
        .createUserWithEmailAndPassword(data.email, password)
        .then((res) => {
            //Buat data baru
            const newData = {
                ...data,
                uid: res.user.uid
            }

            //simpan data ke realtime database
            database().ref(`/users/${res.user.uid}`).set(newData)

            //Success
            dispatchSuccess(dispatch, REGISTER_USER, newData)

            //simpan di local storage
            storeData('userData', newData)

            //pindah ke halaman lain
            navigation.replace('MainApp')

            //loading off
            dispatch(setLoading(false))

        })
        .catch((error) => {
            
            //loading off
            dispatch(setLoading(false))
            
            //error
            dispatchError(dispatch, REGISTER_USER, error.message)

            if (error.code === 'auth/email-already-in-use') {
                return showError('That email address is already in use!');
              }
          
            if (error.code === 'auth/invalid-email') {
                return showError('That email address format is invalid!');
            }

            if (error.code === 'auth/weak-password') {
                return showError('Password should be at least 6 characters');
            }
            return showError(error.message)
        })
    }
}

export const loginUser = (email, password, navigation) => {
    return (dispatch) => {
        //loading on
        dispatch(setLoading(true))

        auth().signInWithEmailAndPassword(email, password)
        .then((res) => {
            database().ref(`/users/${res.user.uid}`).once('value')
            .then((resDB) => {
                //Success
               
                if(resDB.val()) {
                    dispatchSuccess(dispatch, LOGIN_USER, resDB.val())
    
                    //simpan di local storage
                    storeData('userData', resDB.val())

                    // pindah ke halaman lain
                    navigation.replace('MainApp')

                    //loading off
                    dispatch(setLoading(false))

                } else {

                    //loading off
                    dispatch(setLoading(false))
                    
                    dispatchError(dispatch, LOGIN_USER, 'Data user note available')

                    showError('Data user note available')
                }
            })

        })
        .catch((error) => {

            //loading off
            dispatch(setLoading(false))
            
            dispatchError(dispatch, LOGIN_USER, error.message)


            if (error.code === 'auth/invalid-email') {
                return showError('That email address format is invalid!');
            }

            if (error.code === 'auth/wrong-password') {
                return showError('Password incorrect');
            }

            if (error.code === 'auth/user-not-found') {
                return showError('User not found');
            }

            return showError(error.message)
        })
    }
}