import { setLoading } from "./loadingAction"
import database from '@react-native-firebase/database';
import { dispatchError, dispatchLoading, dispatchSuccess } from "../../utils/Dispatch";
import { showError } from "../../utils/showMessage";

export const GET_LIST_JERSEY = 'GET_LIST_JERSEY'
export const GET_LIST_JERSEY_BY_LIGA = 'GET_LIST_JERSEY_BY_LIGA'
export const DELETE_PARAM_JERSEY = 'DELETE_PARAM_JERSEY'
export const SAVE_KEYWORD_JERSEY = 'SAVE_KEYWORD_JERSEY'

export const getListJersey = (idLiga, keyword) => {
    return (dispatch) => {
        dispatchLoading(dispatch, GET_LIST_JERSEY)

        if(idLiga) {
            database()
            .ref('/jerseys')
            .orderByChild('liga')
            .equalTo(idLiga)
            .once('value')
            .then((res) => {
                dispatchSuccess(dispatch, GET_LIST_JERSEY, res.val())
            })
            .catch((error) => {
                dispatchError(dispatch, GET_LIST_JERSEY, error.message)
                showError(error.message)
            })

        } 
        
        else if (keyword) {
            database()
            .ref('/jerseys')
            .orderByChild('klub')
            .startAt(keyword.toUpperCase())
            .endAt(keyword.toUpperCase() + "\uf8ff")
            // .equalTo(keyword.toUpperCase())
            .once('value')
            .then((res) => {
                dispatchSuccess(dispatch, GET_LIST_JERSEY, res.val())
            })
            .catch((error) => {
                dispatchError(dispatch, GET_LIST_JERSEY, error.message)
                showError(error.message)
            })
        }
        
        else {
            database()
            .ref('/jerseys')
            .once('value')
            .then((res) => {
                dispatchSuccess(dispatch, GET_LIST_JERSEY, res.val())
            })
            .catch((error) => {
                dispatchError(dispatch, GET_LIST_JERSEY, error.message)
                showError(error.message)
            })
        }
    }
}

export const getFavoriteJersey = () => {
    return (dispatch) => {
        dispatchLoading(dispatch, GET_LIST_JERSEY)
        database()
        .ref('/jerseys')
        .limitToLast(6)
        .once('value')
        .then((res) => {
            dispatchSuccess(dispatch, GET_LIST_JERSEY, res.val())
        })
        .catch((error) => {
            dispatchError(dispatch, GET_LIST_JERSEY, error.message)
            showError(error.message)
        })
    }
}

export const getJerseyByLiga = (id, namaLiga) => ({
    type: GET_LIST_JERSEY_BY_LIGA,
    payload: {
        idLiga: id,
        namaLiga: namaLiga
    }
})

export const deleteParamJersey = () => ({
    type: DELETE_PARAM_JERSEY
})

export const saveKeywordJersey = (search) => ({
    type: SAVE_KEYWORD_JERSEY,
    payload: {
        data: search
    }
})