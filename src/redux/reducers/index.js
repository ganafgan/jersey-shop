import { combineReducers } from 'redux'
import userReducer from './userAction'
import ongkirReducer from './ongkirAction'
import authReducer from './authAction'
import loadingReducer from './loadingAction'
import profileReducer from './profileAction'
import ligaReducer from './ligaAction'
import jerseyReducer from './jerseyAction'

const reducers = combineReducers({
    userReducer,
    ongkirReducer,
    authReducer,
    loadingReducer,
    profileReducer,
    ligaReducer,
    jerseyReducer
})

export default reducers