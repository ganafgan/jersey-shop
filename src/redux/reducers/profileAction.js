import { UPDATE_PROFILE , UPDATE_AVATAR} from "../actions/profileAction"


const initialState = {
    avatarResult: false,
    avatarError: false,

    profileResult: false,
    profileError: false
}

export default (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_AVATAR:
            return {
                ...state,
                avatarResult: action.payload.data,
                avatarError: action.payload.errorMessage
            }
        case UPDATE_PROFILE:
            return {
                ...state,
                profileResult: action.payload.data,
                profileError: action.payload.errorMessage
            }
        default:
        return state
    } 
}
