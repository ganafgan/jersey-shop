import { REGISTER_USER, LOGIN_USER } from "../actions/authAction";

const initialState = {
    registerResult: false,
    registerError: false,

    loginResult: false,
    loginError: false,


}

export default (state = initialState, action) => {
    switch(action.type) {
        case REGISTER_USER:
            return {
                ...state,
                registerResult: action.payload.data,
                registerError: action.payload.errorMessage
            }
        case LOGIN_USER:
            return {
                ...state,
                loginResult: action.payload.data,
                loginError: action.payload.errorMessage
            } 
        default:
        return state
    } 
}