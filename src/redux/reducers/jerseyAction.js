import { 
    GET_LIST_JERSEY, 
    GET_LIST_JERSEY_BY_LIGA, 
    DELETE_PARAM_JERSEY,
    SAVE_KEYWORD_JERSEY
} 
from "../actions/jerseyAction";

const initialState = {
    listJerseyResult: false,
    listJerseyError: false,
    listJerseyLoading: false,

    idLiga: false,
    namaLiga: false,
    keyword: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_LIST_JERSEY:
            return {
                ...state,
                listJerseyResult: action.payload.data,
                listJerseyError: action.payload.errorMessage,
                listJerseyLoading: action.payload.loading
            }
        case GET_LIST_JERSEY_BY_LIGA:
            return {
                ...state,
                idLiga: action.payload.idLiga,
                namaLiga: action.payload.namaLiga
            }
        case DELETE_PARAM_JERSEY:
            return {
                ...state,
                idLiga: false,
                namaLiga: false,
                keyword: false
            }
        case SAVE_KEYWORD_JERSEY:
            return {
                ...state,
                keyword: action.payload.data
            }
        default:
        return state
    }
}