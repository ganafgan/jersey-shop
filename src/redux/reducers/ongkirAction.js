import { GET_KOTA, GET_PROVINSI } from "../actions/ongkirAction"

const initialState = {
    provinsiLoading: false,
    provinsiResult: false,
    ProvinsiError: false,

    kotaLoading: false,
    kotaResult: false,
    kotaError: false
}

export default (state = initialState, action) => {
    switch(action.type) {
        case GET_PROVINSI:
            return {
                ...state,
                provinsiLoading: action.payload.loading,
                provinsiResult: action.payload.data,
                provinsiError: action.payload.errorMessage
            }
        case GET_KOTA:
            return {
                ...state,
                kotaLoading: action.payload.loading,
                kotaResult: action.payload.data,
                kotaError: action.payload.errorMessage
            }
        default:
        return state
    } 
}

