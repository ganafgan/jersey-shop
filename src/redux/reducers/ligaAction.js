import { GET_LIST_LIGA , GET_DETAIL_LIGA} from "../actions/ligaAction";

const initialState = {
    listLigaResult: false,
    listLigaError: false,
    listLigaLoading: false,

    listDetailLigaResult: false,
    listDetailLigaError: false,
    listDetailLigaLoading: false,

}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_LIST_LIGA:
            return {
                ...state,
                listLigaResult: action.payload.data,
                listLigaError: action.payload.errorMessage,
                listLigaLoading: action.payload.loading
            }
        case GET_DETAIL_LIGA:
            return {
                ...state,
                listDetailLigaResult: action.payload.data,
                listDetailLigaError: action.payload.errorMessage,
                listDetailLigaLoading: action.payload.loading
            }
        default:
        return state
    }
}