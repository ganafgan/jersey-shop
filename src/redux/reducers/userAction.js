import { GET_USER } from "../actions/userAction"

const initialState = {
    dataUser: false
}

export default (state = initialState, action) => {
    switch(action.type) {
        case GET_USER:
            return {
                ...state,
                dataUser: action.payload
            }
        default:
        return state
    } 
}

