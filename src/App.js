import React from 'react'
import AppNavigator from './router/AppNavigator'
import { Provider, useSelector } from 'react-redux'
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import reducers from './redux/reducers';
import FlashMessage from "react-native-flash-message";
import Loading from './components/moleculs/Loading';



const MainApp = () => {
	
	const { loading } = useSelector(state => state.loadingReducer)

	return (
		<>
			<AppNavigator/>

			{/* Flash Message */}
			<FlashMessage position='top' />

			{/* Loading state global */}
			{loading && <Loading/>}
		</>
	)
}

const App = () => {
	const store = createStore(reducers, applyMiddleware(thunk))
	return(
		<Provider store={store}>
			<MainApp/>
		</Provider>
	)
}
export default App

