import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import Swiper from 'react-native-swiper'
import { Colors } from '../../../utils/Colors'
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension'
import { slider1, slider2 } from '../../../assets'
import { SliderBox } from "react-native-image-slider-box";

const BannerSlider = () => {

    const images = [
        slider1,
        slider2
    ]

    return (
        <View style={styles.container}>
            <SliderBox
                images={images}
                dotColor={Colors.primary}
                inactiveDotColor="#90A4AE"
                autoplay
                circleLoop
                sliderBoxHeight={responsiveHeight(132)}
                ImageComponentStyle={styles.slider}
                dotStyle={styles.dot}
                imageLoadingColor={Colors.primary}
            />
        </View>
    )
}

export default BannerSlider

const styles = StyleSheet.create({
    container: {
       marginTop: -15
       
       
    },
    slider: {
        borderRadius: 5,
        width: responsiveWidth(354)
    },
    dot: {
        width: responsiveWidth(10),
        height: responsiveHeight(5),
        borderRadius: 5
    }
})
