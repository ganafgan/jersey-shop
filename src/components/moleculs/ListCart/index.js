import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import CardCart from '../../atoms/CardCart'

const ListCart = ({carts}) => {
    
    return (
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{paddingBottom: 20}}> 
            <View>
                {
                    carts.map((cart) => {
                        return (
                            <CardCart
                                key={cart.id}
                                cart={cart}
                            />
                        )
                    })
                }
            </View>
        </ScrollView>
    )
}

export default ListCart

const styles = StyleSheet.create({})
