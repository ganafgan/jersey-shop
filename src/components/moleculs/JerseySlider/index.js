import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, Modal, BackHandler } from 'react-native'
import ImageViewer from 'react-native-image-zoom-viewer';
import { SliderBox } from "react-native-image-slider-box";
import { Colors } from '../../../utils/Colors';
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension';

const JerseySlider = (props) => {

    const { images } = props
    const [openImage, setOpenImage] = useState(false)
    const [previewImage, setPreviewImage] = useState(false)
    
    const clickPreview = (index) => {
        console.log(index)
        setOpenImage(true)
        setPreviewImage([
            {
                url: images[index],
               
            }
        ])
    }

    return (
        <View>
            <SliderBox
                images={images}
                dotColor={Colors.primary}
                inactiveDotColor="#90A4AE"
                circleLoop
                sliderBoxHeight={responsiveHeight(300)}
                ImageComponentStyle={styles.slider}
                dotStyle={styles.dot}
                imageLoadingColor={Colors.primary}
                onCurrentImagePressed={(index) => clickPreview(index)}
            />
            <Modal visible={openImage} transparent={true}>

                <ImageViewer 
                    imageUrls={previewImage} 
                    backgroundColor={Colors.white}
                    onClick={() => setOpenImage(false)}
                    enableImageZoom
                    backgroundColor="#90A4AE"
                    useNativeDriver={true}
                    onRequestClose={() => setOpenImage(false)}
                />
            </Modal>
        </View>
    )
}

export default JerseySlider

const styles = StyleSheet.create({
    slider: {
        marginTop: responsiveHeight(30),
        width: responsiveWidth(250),
    },
})
