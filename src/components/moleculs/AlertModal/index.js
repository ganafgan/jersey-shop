import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Modal from "react-native-modal";
import { Colors } from '../../../utils/Colors';
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension';
import { Fonts } from '../../../utils/Fonts';

const AlertModal = ({isVisible, onBackButtonPress, onBackdropPress, text}) => {
    return (
       
            <Modal
                isVisible={isVisible}
                backdropColor={Colors.backdrop}
                onBackButtonPress={onBackButtonPress}
                onBackdropPress={onBackdropPress}
                useNativeDriver={true}
                style={{ marginHorizontal: responsiveWidth(30), flex: 1}}
                animationIn={'zoomIn'}
                animationOut={'zoomOut'}
                hideModalContentWhileAnimating={true}
            >
                <View style={styles.wrapperContent}>
                    <View style={styles.content}>

                        <Text style={styles.text}>{text}</Text>
                    </View>
                </View>
            </Modal>
        
    )
}

export default AlertModal

const styles = StyleSheet.create({
    wrapperContent: {
        backgroundColor: Colors.white,
        paddingHorizontal: responsiveWidth(20),
        paddingVertical: responsiveHeight(20),
        borderRadius: 5,
    },
    content: {

    },
    text: {
        fontFamily: Fonts.primary[400],
        textAlign: 'center'
    }
})
