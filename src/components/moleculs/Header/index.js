import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'
import { Colors } from '../../../utils/Colors'
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension'
import Icon from 'react-native-vector-icons/Ionicons'
import { Button, Gap } from '../../atoms'
import { Fonts } from '../../../utils/Fonts'
import { saveKeywordJersey } from '../../../redux/actions/jerseyAction'


const Header = ({navigation, page, dispatch}) => {

    const [search, setSearch] = useState('')
    
    const handleSubmit = () => {

        //jalankan action save keyword
        dispatch(saveKeywordJersey(search))

        //jika itu halaman home navigate ke list jersey
        if (page !== 'Jersey') {
            navigation.navigate('Jersey')
        }

        //kembalikan state search ke string kosong
        setSearch('')
    }


    return (
        <View style={styles.container}>
            <View style={styles.wrapperHeader}>
                <View style={styles.wrapperInput}>
                    <Icon name='search-sharp' color={Colors.grey1} size={25}/>
                    <TextInput
                        placeholder='Cari jersey...'
                        placeholderTextColor={Colors.grey1}
                        style={styles.input}
                        value={search}
                        onChangeText={(value) => setSearch(value)}
                        onSubmitEditing={handleSubmit}
                    />
                </View>
                <Gap width={10}/>
                <Button
                    icon={<Icon name='cart-sharp' color={Colors.grey1} size={25}/>}
                    padding={10}
                    onPress={() => navigation.navigate('Cart')}
                    color={Colors.white}
                />
            </View>
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.primary,
        height: responsiveHeight(125)
    },
    wrapperHeader: {
        marginTop: 15,
        marginHorizontal: responsiveWidth(30),
        flexDirection: 'row',
        alignItems: 'center',
    },
    wrapperInput: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: Colors.white,
        borderRadius: 5,
        alignItems: 'center',
        paddingLeft: 10
    },
    input: {
        flex: 1,
        fontFamily: Fonts.primary[400],
        fontSize: 12,
    }
})
