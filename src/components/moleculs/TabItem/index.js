import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Colors } from '../../../utils/Colors'
import Icon from 'react-native-vector-icons/Ionicons'
import { Fonts } from '../../../utils/Fonts'

const TabItem = ({isFocused, onLongPress, onPress, label}) => {

    const TabIcon = () => {
        switch (label) {
            case 'Home':
                return isFocused ? <Icon name='navigate-sharp' color={Colors.icon.active} size={25}/> : <Icon name='navigate-sharp' color={Colors.icon.inactive} size={25}/>
            case 'Jersey':
                return isFocused ? <Icon name='shirt-sharp' color={Colors.icon.active} size={25}/> : <Icon name='shirt-sharp' color={Colors.icon.inactive} size={25}/>
            case 'Profile':
                return isFocused ? <Icon name='person-sharp' color={Colors.icon.active} size={25}/> : <Icon name='person-sharp' color={Colors.icon.inactive} size={25}/>
            default:
            return isFocused ? <Icon name='person-sharp' color={Colors.icon.active} size={25}/> : <Icon name='person-sharp' color={Colors.icon.inactive} size={25}/>
        }
    }

    return (
        <TouchableOpacity
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.container}
          >
            <TabIcon/>
            <Text style={styles.label(isFocused)}>{label}</Text>
          </TouchableOpacity>
    )
}

export default TabItem

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    label: (isFocused) => ({
        color: isFocused ? Colors.icon.active : Colors.icon.inactive,
        fontSize: 12,
        marginTop: 4,
        fontFamily: Fonts.primary[400]
    })
})
