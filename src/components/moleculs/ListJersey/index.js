import React from 'react'
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import { RFValue } from 'react-native-responsive-fontsize'
import { useSelector } from 'react-redux'
import { Colors } from '../../../utils/Colors'
import { heightMobileUI } from '../../../utils/Constant'
import { Fonts } from '../../../utils/Fonts'
import { CardJersey } from '../../atoms'

const ListJersey = ({navigation}) => {

    const { listJerseyResult, listJerseyLoading } = useSelector(state => state.jerseyReducer)

    return (
        <View style={styles.container}>
            {
                listJerseyResult ? 
                Object.keys(listJerseyResult).map((key) => {
                        return (
                            <CardJersey
                                jersey={listJerseyResult[key]}
                                key={key}
                                navigation={navigation}
                            />
                        )
                    }) 
                :
                listJerseyLoading ?
                <View style={{ flex: 1,marginTop: 10, marginBottom: 30}}>
                    <ActivityIndicator color={Colors.primary} size='small'/>
                </View> : 
                <View style={{ flex: 1,marginTop: 10, marginBottom: 30, alignItems: 'center'}}>
                    <Text style={{
                        fontFamily: Fonts.primary[500],
                        fontSize: RFValue(18, heightMobileUI),
                        color: Colors.primary
                        
                    }}>
                        Data not available
                    </Text>
                </View>
            }
        </View>
    )
}

export default ListJersey

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    }
})
