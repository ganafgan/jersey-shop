import React from 'react'
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'
import { Colors } from '../../../utils/Colors'
import { Fonts } from '../../../utils/Fonts'

const Loading = () => {
    return (
        <View style={styles.container}>
            <View style={styles.wrapperLoading}>
                <ActivityIndicator
                    size='small'
                    color={Colors.primary}
                />
                <Text style={styles.loading}>Loading...</Text>
            </View>
        </View>
    )
}

export default Loading

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        flex:1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loading: {
        fontSize: 14,
        fontFamily: Fonts.primary[500],
        color: Colors.primary,
        marginTop: 5
    },
    wrapperLoading: {
        backgroundColor: Colors.white,
        borderRadius: 5,
        padding: 10
    }
})
