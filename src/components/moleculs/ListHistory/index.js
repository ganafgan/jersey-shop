import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension'
import { CardHistory } from '../../atoms'


const ListHistory = ({pesanans}) => {
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
                {
                    pesanans.map((pesanan) => {
                        return (
                            <CardHistory
                                pesanan={pesanan}
                                key={pesanan.id}
                            />
                        )
                    })
                }
                
            </View>
        </ScrollView>
    )
}

export default ListHistory

const styles = StyleSheet.create({
    container: {
        marginHorizontal: responsiveWidth(30),
        marginVertical: responsiveHeight(30),
    }
})
