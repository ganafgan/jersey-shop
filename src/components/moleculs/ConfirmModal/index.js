import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Modal from "react-native-modal";
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';
import { Button, Gap } from '../..';
import { Colors } from '../../../utils/Colors';
import { heightMobileUI } from '../../../utils/Constant';
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension';
import { Fonts } from '../../../utils/Fonts';

const ConfirmModal = ({isVisible, onBackButtonPress, onBackdropPress, title, text, onCancel, onOk}) => {
    return (
       
            <Modal
                isVisible={isVisible}
                backdropColor={Colors.backdrop}
                onBackButtonPress={onBackButtonPress}
                onBackdropPress={onBackdropPress}
                useNativeDriver={true}
                style={{ marginHorizontal: responsiveWidth(30), flex: 1}}
                animationIn={'zoomIn'}
                animationOut={'zoomOut'}
                hideModalContentWhileAnimating={true}
            >
                <View style={styles.wrapperContent}>
                    <View style={styles.content}>
                        <Text style={styles.title}>{title}</Text>
                        <Gap height={responsiveHeight(30)}/>
                        <Text style={styles.text}>{text}</Text>
                    </View>
                    <Gap height={responsiveHeight(30)}/>
                    <View style={styles.wrapperButton}>
                        <View style={{width: '45%'}}>
                            <Button
                                type='text'
                                padding={10}
                                title='Cancel'
                                typeBtn='secondary'
                                onPress={onCancel}
                            />
                        </View>
                        <View style={{width: '45%'}}>
                            <Button
                                type='text'
                                padding={10}
                                title='OK'
                                onPress={onOk}
                            />
                        </View>
                    </View>
                </View>
            </Modal>
        
    )
}

export default ConfirmModal

const styles = StyleSheet.create({
    wrapperContent: {
        backgroundColor: Colors.white,
        paddingHorizontal: responsiveWidth(20),
        paddingVertical: responsiveHeight(20),
        borderRadius: 5,
    },
    content: {

    },
    title: {
        fontFamily: Fonts.primary[500],
        textAlign: 'center',
        color: Colors.primary,
        fontSize: RFValue(18, heightMobileUI)
    },
    text: {
        fontFamily: Fonts.primary[500],
        textAlign: 'center',
        color: Colors.black,
        fontSize: RFValue(16, heightMobileUI)
    },
    wrapperButton: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})
