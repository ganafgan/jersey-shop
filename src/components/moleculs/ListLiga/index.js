import React from 'react'
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'
import { RFValue } from 'react-native-responsive-fontsize'
import { useSelector } from 'react-redux'
import { Colors } from '../../../utils/Colors'
import { heightMobileUI } from '../../../utils/Constant'
import { responsiveHeight } from '../../../utils/Dimension'
import { Fonts } from '../../../utils/Fonts'
import { CardLiga } from '../../atoms'

const ListLiga = ({navigation, dispatch}) => {

    const { listLigaResult, listLigaLoading } = useSelector(state => state.ligaReducer)
    
    return (
        <View style={styles.container}>
            {
                listLigaResult ? 
                Object.keys(listLigaResult).map((key) => {
                        return (
                            <CardLiga
                                liga={listLigaResult[key]} 
                                key={key}
                                id={key}
                                navigation={navigation}
                                dispatch={dispatch}
                            />
                        )
                    }) 
                :
                listLigaLoading ?
                <View style={{ flex: 1,marginTop: 10, marginBottom: 30}}>
                    <ActivityIndicator color={Colors.primary} size='small'/>
                </View> : 
                 <View style={{ flex: 1,marginTop: 10, marginBottom: 30, alignItems: 'center'}}>
                    <Text style={{
                        fontFamily: Fonts.primary[500],
                        fontSize: RFValue(18, heightMobileUI),
                        color: Colors.primary
                        
                    }}>
                     Data not available
                    </Text>
                </View>
            }
        </View>
    )
}

export default ListLiga

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: responsiveHeight(10)
    }
})
