import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../../../utils/Colors'
import { Fonts } from '../../../utils/Fonts'
import Loading from './Loading'
import TextOnly from './TextOnly'

const Button = (props) => {

    const { icon, total, padding, type, title, onPress, color, loading, disabled } = props

    if (loading){
        return <Loading {...props}/>
    }
    if (disabled) {
        return (
            <View style={styles.btn(padding, color, disabled)} onPress={onPress}>
                <Text>{icon}</Text>
                <Text style={styles.title}>   {title}</Text>
            </View>
        )
    }
    if (type == 'text') {
        return <TextOnly {...props}/>
    }
    if (type == 'textIcon') {
        return (
            <TouchableOpacity activeOpacity={0.7} style={styles.btn(padding, color)} onPress={onPress}>
                <Text>{icon}</Text>
                <Text style={styles.title}>   {title}</Text>
            </TouchableOpacity>
        )
    }
    return (
       <TouchableOpacity activeOpacity={0.7} style={[styles.btn(padding, color)]} onPress={onPress}>
           <Text>{icon}</Text>
           {
               total && (
                <View style={styles.total}>
                    <Text style={styles.textTotal}>{total}</Text>
                </View>
               )
           }
       </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    btn: (padding, color, disabled) => ({
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: color,
        padding: padding,
        borderRadius: 5,
        opacity: disabled ? 0.5 : 1
    }),
    total: {
        position: 'absolute',
        top: 5,
        right: 5,
        backgroundColor: 'red',
        borderRadius: 3,
        padding: 3
    },
    title: {
        fontFamily: Fonts.primary[400],
        color: Colors.white
    },
    textTotal: {
        fontFamily: Fonts.primary[400],
        color: Colors.white
    }
})
