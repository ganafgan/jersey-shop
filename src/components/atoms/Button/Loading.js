import React from 'react'
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../../../utils/Colors'
import { Fonts } from '../../../utils/Fonts'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { heightMobileUI } from '../../../utils/Constant';
import { Gap } from '..';
import { responsiveWidth } from '../../../utils/Dimension';

const Loading = ({padding, title, onPress}) => {

    return (
       <TouchableOpacity activeOpacity={0.7} style={styles.btn(padding)} onPress={onPress}>
           <ActivityIndicator size='small' color={Colors.white}/>
           <Gap width={responsiveWidth(5)}/>
           <Text style={styles.text}>Loading</Text>
       </TouchableOpacity>
    )
}

export default Loading

const styles = StyleSheet.create({
    btn: (padding) => ({
        backgroundColor: Colors.disabled,
        padding: padding,
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }),
    text: {
        color: Colors.white,
        textAlign: 'center',
        fontFamily: Fonts.primary[400],
        fontSize: RFValue(18, heightMobileUI)
    }
    
})
