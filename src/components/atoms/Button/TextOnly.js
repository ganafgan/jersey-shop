import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../../../utils/Colors'
import { Fonts } from '../../../utils/Fonts'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { heightMobileUI } from '../../../utils/Constant';

const TextOnly = ({padding, title, onPress, typeBtn}) => {

    if (typeBtn == 'secondary') {
        return (
            <TouchableOpacity activeOpacity={0.7} style={styles.btnScn(padding)} onPress={onPress}>
                <Text style={styles.textScn}>{title}</Text>
            </TouchableOpacity>
         )
    }
    return (
       <TouchableOpacity activeOpacity={0.7} style={styles.btn(padding)} onPress={onPress}>
           <Text style={styles.text}>{title}</Text>
       </TouchableOpacity>
    )
}

export default TextOnly

const styles = StyleSheet.create({
    btnScn: (padding) => ({
        padding: padding,
        backgroundColor: Colors.white,
        borderRadius: 5,
        borderColor: Colors.primary,
        borderWidth: 1
    }),
    btn: (padding) => ({
        padding: padding,
        backgroundColor: Colors.primary,
        borderRadius: 5,
    }),
    text: {
        color: Colors.white,
        textAlign: 'center',
        fontFamily: Fonts.primary[400],
        fontSize: RFValue(16, heightMobileUI)
    },
    textScn:  {
        color: Colors.primary,
        textAlign: 'center',
        fontFamily: Fonts.primary[400],
        fontSize: RFValue(16, heightMobileUI)
    }
    
})
