import React, { useState } from 'react'
import { StyleSheet, View, Text, TextInput } from 'react-native'
import { RFValue } from 'react-native-responsive-fontsize'
import { Colors } from '../../../utils/Colors'
import { heightMobileUI } from '../../../utils/Constant'
import { responsiveHeight } from '../../../utils/Dimension'
import { Fonts } from '../../../utils/Fonts'

const Input = ({textarea, fontSize, width, height, placeHolder, label, value, secureTextEntry, keyboardType, onChangeText, disabled}) => {
    
    const [color, setColor] = useState(Colors.disabled)
   
    const onBlur = () => {
        setColor(Colors.disabled)
    }

    const onFocus = () => {
        setColor(Colors.primary)
    }

    if (textarea) {
        return (
            <View style={styles.container}>
                <Text style={styles.label(fontSize)}>{label} :</Text>
                <TextInput
                    style={styles.inputTextArea(fontSize, color)}
                    multiline={true}
                    numberOfLines={4}
                    value={value}
                    onChangeText={onChangeText}
                    onBlur={onBlur}
                    onFocus={onFocus}
                    editable={disabled ? false : true}
                />
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <Text style={styles.label(fontSize)}>{label} :</Text>
            <TextInput
                style={styles.input(fontSize, width, height, color)}
                placeholder={placeHolder}
                value={value}
                secureTextEntry={secureTextEntry}
                keyboardType={keyboardType}
                onChangeText={onChangeText}
                onBlur={onBlur}
                onFocus={onFocus}
                editable={disabled ? false : true}
            />
        </View>
    )
}

export default Input

const styles = StyleSheet.create({
    container: {
    },
    label: (fontSize) => ({
        fontSize: fontSize,
        fontFamily: Fonts.primary[500]
    }),
    input: (fontSize, width, height, color) => ({
        fontSize: fontSize ,
        fontFamily: Fonts.primary[500],
        height: height,
        width: width,
        borderWidth:0.5,
        borderRadius: 5,
        borderColor: color,
        paddingVertical: 5,
        paddingHorizontal: 10
    }),
    inputTextArea: (fontSize, color) => ({
        fontSize: fontSize ,
        fontFamily: Fonts.primary[500],
        borderWidth:0.5,
        borderRadius: 5,
        borderColor: color,
        paddingVertical: 5,
        paddingHorizontal: 10,
        textAlignVertical: 'top'
    }),
})
