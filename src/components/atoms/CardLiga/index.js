import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../../../utils/Colors'
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension'
import { useDispatch, useSelector } from 'react-redux'
import { getJerseyByLiga } from '../../../redux/actions/jerseyAction'

const CardLiga = ({liga, navigation, id, dispatch}) => {
    
    const toJerseyByLiga = (id, namaLiga) => {

        // ke jersey action
        dispatch(getJerseyByLiga(id, namaLiga))

        //navigate ke list jersey
       navigation.navigate('Jersey')
    
    }

    return (
        <TouchableOpacity 
            style={styles.container} 
            activeOpacity={0.7} 
            onPress={() => toJerseyByLiga(id, liga.namaLiga)}
        >
            <Image
                source={{uri: liga.image}}
                style={styles.img}
            />
        </TouchableOpacity>
    )
}

export default CardLiga

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal: 5,
        paddingVertical: 10,
        borderRadius: 5
    },
    img: {
        height: responsiveHeight(50),
        width: responsiveWidth(60),
        resizeMode: 'contain'
    }
})
