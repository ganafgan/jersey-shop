import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { RFValue } from 'react-native-responsive-fontsize'
import { Gap } from '..'
import { Colors } from '../../../utils/Colors'
import { heightMobileUI, numberWithCommas } from '../../../utils/Constant'
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension'
import { Fonts } from '../../../utils/Fonts'

const CardHistory = ({pesanan}) => {
   
    return (
        <View style={styles.container}>
            <Text style={styles.date}>{pesanan.tanggalPemesanan}</Text>
            {pesanan.pesanans.map((history, index) => {
                return (
                    <View key={index} style={styles.content}>
                        <Text style={styles.number}>{index + 1}.</Text>
                        <Image
                            source={history.product.gambar[0]}
                            style={styles.image}
                        />
                        <View style={styles.desc}>
                            <Text style={styles.nama}>{history.product.nama}</Text>
                            <Text style={styles.price}>Rp. {numberWithCommas(history.product.harga)}</Text>
                            <Gap height={responsiveHeight(10)}/>
                            <Text style={styles.jumlah}>Pesanan : {history.jumlahPesan}</Text>
                            <Text style={styles.total}>Total Harga : Rp. {history.totalHarga}</Text>
                        </View>
                    </View>
                )
            })}
            <Gap height={responsiveHeight(20)}/>
            <View style={styles.footer}>
                <View style={styles.label}>
                    <Text style={styles.textBlue}>Status :</Text>
                    <Text style={styles.textBlue}>Ongkir (2-3 Hari) :</Text>
                    <Text style={styles.textBlue}>Total Harga</Text>
                </View>
                <View>
                    <Text style={styles.textBlue}>{pesanan.status}</Text>
                    <Text style={styles.textBlue}>Rp. 15000</Text>
                    <Text style={styles.textBlue}>Rp. {numberWithCommas(pesanan.totalHarga + 15000)}</Text>
                </View>
            </View>
        </View>
    )
}

export default CardHistory

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 5,
        marginBottom: responsiveHeight(20)
    },
    date: {
        fontFamily: Fonts.primary[500],
        fontSize: RFValue(18, heightMobileUI)
    },
    content: {
        flexDirection: 'row',
        marginTop: responsiveHeight(10)
    },
    number: {
        fontFamily: Fonts.primary[600],
        fontSize: RFValue(16, heightMobileUI)
    },
    nama: {
        fontFamily: Fonts.primary[400],
        fontSize: RFValue(16, heightMobileUI),
        textTransform: 'capitalize'
    },
    image: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        resizeMode: 'contain'
    },
    desc: {
        marginLeft: responsiveWidth(5)
    },
    price: {
        fontFamily: Fonts.primary[400],
        fontSize: RFValue(16, heightMobileUI)
    },
    jumlah: {
        fontFamily: Fonts.primary[400],
        fontSize: RFValue(16, heightMobileUI)
    },
    total: {
        fontFamily: Fonts.primary[400],
        fontSize: RFValue(16, heightMobileUI)
    },
    footer: {
        flexDirection: 'row'
    },
    label: {
        flex: 1
    },
    textBlue: {
        fontFamily: Fonts.primary[600],
        fontSize: RFValue(18, heightMobileUI),
        color: Colors.grey1,
        textTransform: 'uppercase',
    }
})
