import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { Colors } from '../../../utils/Colors'
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension'
import { RFValue } from "react-native-responsive-fontsize";
import { heightMobileUI } from '../../../utils/Constant'
import { Fonts } from '../../../utils/Fonts'

const CardMenu = ({onPress, nama, gambar}) => {
   

    return (
        <TouchableOpacity style={styles.container} activeOpacity={0.7} onPress={onPress}>
            <View>
                {gambar}
            </View>
            <Text style={styles.nama}>{nama}</Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Icon name='chevron-forward' size={20} color={'#c4c4c4'}/>
            </View>
        </TouchableOpacity>
    )
}

export default CardMenu

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        backgroundColor: Colors.white,
        marginHorizontal: responsiveWidth(30),
        marginVertical: responsiveHeight(20),
        padding: responsiveHeight(15),
        borderRadius: 5,
      
    },
    nama: {
        fontSize: RFValue(18, heightMobileUI),
        fontFamily: Fonts.primary[500],
        marginLeft: responsiveWidth(10)
    }
})
