import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput } from 'react-native'
import { Colors } from '../../../utils/Colors'
import { Fonts } from '../../../utils/Fonts'
import { Picker } from '@react-native-picker/picker';
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension';

const Options = ({label, datas, width, height, fontSize, selectedValue, onValueChange}) => {


    return (
        <View style={styles.container}>
            <Text style={styles.label(fontSize)}>{label}</Text>
            <View style={styles.wrapperPicker}>
                <Picker
                    selectedValue={selectedValue}
                    style={styles.picker(width, height, fontSize)}
                    onValueChange={onValueChange}
                >
                    <Picker.Item label='-- Pilih --' value='' />
                    {
                        datas && datas.map((res, index) => {
                            switch (label) {
                                case 'Provinsi':
                                   return <Picker.Item label={res.province} value={res.province_id} key={res.province_id} />
                                case 'Kota/Kabupaten':
                                    return <Picker.Item label={`${res.type} ${res.city_name}`} value={res.city_id} key={res.city_id} />
                                default:
                                    return  <Picker.Item label={res} value={res} key={index} />
                            }
                        })
                    }
                </Picker>

            </View>
        </View>
    )
}

export default Options

const styles = StyleSheet.create({
    container: {
       
    },
    label: (fontSize) => ({
        fontSize: fontSize,
        fontFamily: Fonts.primary[500]
    }),
    wrapperPicker: {
       
        borderWidth:0.5,
        borderRadius: 5,
        borderColor: Colors.grey1,
    },
    picker: (width, height, fontSize) => ({
        fontSize: fontSize,
        fontFamily: Fonts.primary[500],
        height: height ? height : responsiveHeight(46),
        width: width,
        marginTop: -responsiveHeight(12),
        marginBottom: responsiveHeight(12)
        
    }),
})
