import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../../../utils/Colors'
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension'
import { Fonts } from '../../../utils/Fonts'
import Icon from 'react-native-vector-icons/Ionicons'
import { numberWithCommas } from '../../../utils/Constant'

const CardCart = ({cart}) => {
    return (
        <View style={styles.container}>
            <View>
                <Image 
                    source={cart.product.gambar[0]}
                    style={styles.img}
                />
            </View>
           <View style={{marginLeft: responsiveWidth(10), width: '60%'}} >
                <Text style={styles.nama}>{cart.product.nama}</Text>
                <Text style={styles.harga}>Harga: {numberWithCommas(cart.product.harga)}</Text>
                <Text style={styles.pesanan}>Pesan: {cart.jumlahPesan}</Text>
                <Text style={styles.ukuran}>Ukuran: {cart.ukuran}</Text>
                <Text style={styles.totalHarga}>Total: {numberWithCommas(cart.totalHarga)}</Text>
                <Text style={styles.keterangan}>Keterangan: {cart.keterangan}</Text>
           </View>
           <View>
               <TouchableOpacity>
                    <Icon  name='close' size={30} color={Colors.red}/>
               </TouchableOpacity>
           </View>
        </View>
    )
}

export default CardCart

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        backgroundColor: Colors.white,
        marginHorizontal: responsiveWidth(30),
        marginTop: responsiveHeight(20),
        padding: responsiveHeight(10),
        borderRadius: 5
    },
    img: {
        height: responsiveHeight(120),
        width: responsiveWidth(85),
        resizeMode: 'cover',
    },
    nama: {
        fontSize: 14,
        fontFamily: Fonts.primary[600],
        color: Colors.black,
        marginBottom: responsiveHeight(0)
    },
    harga: {
        fontSize: 12,
        fontFamily: Fonts.primary[400],
        color: Colors.black
    },
    pesanan: {
        fontSize: 12,
        fontFamily: Fonts.primary[400],
        color: Colors.black
    },
    ukuran: {
        fontSize: 12,
        fontFamily: Fonts.primary[400],
        color: Colors.black
    },
    totalHarga: {
        fontSize: 12,
        fontFamily: Fonts.primary[400],
        color: Colors.black
    },
    keterangan: {
        fontSize: 12,
        fontFamily: Fonts.primary[400],
        color: Colors.black
    }
})
