import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../../../utils/Colors'
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension'
import { Fonts } from '../../../utils/Fonts'
import Button from '../Button'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { heightMobileUI } from '../../../utils/Constant'

const CardJersey = ({jersey, navigation}) => {
   
    return (
        <View style={styles.container}>
            <View style={styles.card}>
                <Image
                    source={{uri: jersey.gambar[0]}}
                    style={styles.img}
                />
                <Text style={styles.title}>{jersey.nama}</Text>
            </View>
            <Button 
                type='text'
                title='Detail'
                padding={5}
                onPress={() => navigation.navigate('JerseyDetail', {jersey: jersey})}
            />
        </View>
    )
}

export default CardJersey

const styles = StyleSheet.create({
    container: {
        marginBottom: 20
    },
    card: {
        backgroundColor: Colors.white,
        width: responsiveWidth(150),
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        padding: 10,
        borderRadius: 5,
        marginBottom: responsiveHeight(10)
    },
    img: {
        height:responsiveHeight(124),
        width: responsiveWidth(124),
        resizeMode: 'contain'
    },
    title: {
        fontSize: RFValue(14,heightMobileUI),
        fontFamily: Fonts.primary[500],
        textTransform: 'capitalize',
        textAlign: 'center'
        
    }
})
