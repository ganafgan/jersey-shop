import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { RFValue } from 'react-native-responsive-fontsize'
import { Colors } from '../../../utils/Colors'
import { heightMobileUI } from '../../../utils/Constant'
import { responsiveHeight, responsiveWidth } from '../../../utils/Dimension'
import { Fonts } from '../../../utils/Fonts'

const CardAddress = ({address, onPress}) => {
 
    return (
        <View style={styles.container}>
            <Text style={styles.address}>Alamat Saya</Text>
            <Text style={styles.value}>{address.alamat}</Text>
            <Text style={styles.value}>{address.kota}</Text>
            <Text style={styles.value}>{address.provinsi}</Text>
            <TouchableOpacity onPress={onPress}>
                <Text style={styles.link}>Ubah Alamat</Text>
            </TouchableOpacity>
        </View>
    )
}

export default CardAddress

const styles = StyleSheet.create({
    container: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        backgroundColor: Colors.white,
        marginTop: responsiveHeight(20),
        padding: responsiveHeight(15),
        borderRadius: 5
    },
    address: {
        fontFamily: Fonts.primary[500],
        fontSize: RFValue(18, heightMobileUI),
        marginBottom: responsiveHeight(10)
    },
    value: {
        fontFamily: Fonts.primary[500],
        fontSize: RFValue(18, heightMobileUI),
        color: Colors.grey1,
       
    },
    link: {
        fontFamily: Fonts.primary[600],
        fontSize: RFValue(18, heightMobileUI),
        color:Colors.primary,
        textAlign: 'right'
    }
})
