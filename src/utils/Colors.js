const mainColors = {
    violet1: '#6236ff',
    violet2: '#502CD1',
    white1: '#fff',
    black1: '#000',
    grey1: '#7D8797',
    grey2: '#d1d1d1',
    grey3: '#444',
    red1: '#ff0000',
    red2: '#E06379',
}

export const Colors = {
    white: mainColors.white1,
    black: mainColors.black1,
    primary: mainColors.violet1,
    icon: {
        active: mainColors.white1,
        inactive: mainColors.violet2 
    },
    grey1: mainColors.grey1,
    red: mainColors.red1,
    error: mainColors.red2,
    disabled: mainColors.grey2,
    backdrop: mainColors.grey3
}