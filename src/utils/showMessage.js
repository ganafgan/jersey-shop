import { showMessage } from "react-native-flash-message"
import { Colors } from "./Colors"
import { Fonts } from "./Fonts"


export const showError = (message) => {
    showMessage({
        message: message,
        type: 'default',
        backgroundColor: Colors.error,
        color: Colors.white,
        duration: 3000,
        titleStyle:{fontFamily: Fonts.primary[500]}
    })
}

export const showSuccess = (message) => {
    showMessage({
        message: message,
        type: 'success',
        color: Colors.white,
        duration: 3000,
        titleStyle:{fontFamily: Fonts.primary[500]}
    })
}