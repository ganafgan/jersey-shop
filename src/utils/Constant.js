export const heightMobileUI = 896
export const widthMobileUI = 414

export const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const API_KEY = '5c9ae58dcbb34b8abbfc9d2ab7d12fd6'

export const API_RAJAONGKIR = 'https://api.rajaongkir.com/starter/'

export const API_TIMEOUT = 120000

export const API_HEADER_RAJAONGKIR = {
    'key': API_KEY
} 

export const dummyAvatar_url = `https://firebasestorage.googleapis.com/v0/b/jerseypedia-4ecb5.appspot.com/o/dummyAvatar.png?alt=media&token=7b0a88e6-31ff-4764-a5a3-942376344eb6`
