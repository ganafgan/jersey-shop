import Logo from './logo.svg'
import slider1 from './slider1.png'
import slider2 from './slider2.png'
import BundesLiga from './bundesliga.png'
import Laliga from './laliga.png'
import PremierLeague from './premierleague.png'
import Seriea from './seriea.png'
import ProfilePict from './profile.jpg'
import dummyAvatar from './dummyAvatar.png'

export {
    Logo,
    slider1,
    slider2,
    BundesLiga,
    Laliga,
    PremierLeague,
    Seriea,
    ProfilePict,
    dummyAvatar
}