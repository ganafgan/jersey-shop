import { BundesLiga, Laliga, PremierLeague, Seriea } from "../../images";
import { 
    ChelseaBack,
    ChelseaFront,
    LeicesterBack,
    LeicesterFront,
    LiverpoolBack,
    LiverpoolFront,
    MilanFront,
    MilanBack,
    JuveFront,
    JuveBack,
    DortmundFront,
    DortmundBack,
    MunchenFront,
    MunchenBack,
    MadridFront,
    MadridBack

} from "../../jersey";

export const dummyJerseys = [
    {
      id: 1,
      nama: 'CHELSEA 3RD KIT 2018-2019',
      gambar: [ChelseaFront, ChelseaBack],
      liga: {
        id: 2,
        nama: 'Premier League',
        gambar: PremierLeague,
      },
      harga: 125000,
      berat: 0.25,
      jenis: 'Replika Top Quality',
      ukuran: ["S", "M", "L", "XL", "XXL"],
      ready: true
    },
    {
      id: 2,
      nama: 'DORTMUND AWAY 2018-2019',
      gambar: [DortmundFront, DortmundBack],
      liga: {
        id: 4,
        nama: 'Bundesliga',
        gambar: BundesLiga,
      },
      harga: 125000,
      berat: 0.25,
      jenis: 'Replika Top Quality',
      ukuran: ["S", "M", "L", "XL", "XXL"],
      ready: true
    },
    {
      id: 3,
      nama: 'JUVENTUS AWAY 2018-2019',
      gambar: [JuveFront, JuveBack],
      liga: {
        id: 3,
        nama: 'Serie A',
        gambar: Seriea,
      },
      harga: 125000,
      berat: 0.25,
      jenis: 'Replika Top Quality',
      ukuran: ["S", "M", "L", "XL", "XXL"],
      ready: true
    },
    {
      id: 4,
      nama: 'LEICESTER CITY HOME 2018-2019',
      gambar: [LeicesterFront, LeicesterBack],
      liga: {
        id: 2,
        nama: 'Premier League',
        gambar: PremierLeague,
      },
      harga: 125000,
      berat: 0.25,
      jenis: 'Replika Top Quality',
      ukuran: ["S", "M", "L", "XL", "XXL"],
      ready: true
    },
    {
      id: 5,
      nama: 'LIVERPOOL AWAY 2018-2019',
      gambar: [LiverpoolFront, LiverpoolBack],
      liga: {
        id: 2,
        nama: 'Premier League',
        gambar: PremierLeague,
      },
      harga: 125000,
      berat: 0.25,
      jenis: 'Replika Top Quality',
      ukuran: ["S", "M", "L", "XL", "XXL"],
      ready: true
    },
    {
      id: 6,
      nama: 'REAL MADRID 3RD 2018-2019',
      gambar: [MadridFront, MadridBack],
      liga: {
        id: 1,
        nama: 'La Liga',
        gambar: Laliga,
      },
      harga: 125000,
      berat: 0.25,
      jenis: 'Replika Top Quality',
      ukuran: ["S", "M", "L", "XL", "XXL"],
      ready: true
    },
    {
      id: 7,
      nama: 'AC MILAN HOME 2018 2019',
      gambar: [MilanFront, MilanBack],
      liga: {
        id: 3,
        nama: 'Serie A',
        gambar: Seriea,
      },
      harga: 125000,
      berat: 0.25,
      jenis: 'Replika Top Quality',
      ukuran: ["S", "M", "L", "XL", "XXL"],
      ready: true
    },
    {
      id: 8,
      nama: 'BAYERN MUNCHEN 3RD 2018 2019',
      gambar: [MunchenFront, MunchenBack],
      liga: {
        id: 4,
        nama: 'Bundesliga',
        gambar: BundesLiga,
      },
      harga: 125000,
      berat: 0.25,
      jenis: 'Replika Top Quality',
      ukuran: ["S", "M", "L", "XL", "XXL"],
      ready: true
    },
  ];