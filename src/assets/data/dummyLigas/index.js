import { BundesLiga, Laliga, PremierLeague, Seriea } from "../../images";

export const dummyLiga = [
    {
        id: 1,
        nama: 'Premier League',
        gambar: PremierLeague
    },
    {
        id: 2,
        nama: 'Serie A',
        gambar: Seriea
    },
    {
        id: 3,
        nama: 'La Liga',
        gambar: Laliga
    },
    {
        id: 4,
        nama: 'Bundes Liga',
        gambar: BundesLiga
    }
]