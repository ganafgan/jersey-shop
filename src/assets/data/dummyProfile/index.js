import { ProfilePict } from "../../images";

export const dummyProfile = {
    nama: 'Samantha Joana',
    email: 'samanthajo@gmail.com',
    nomorHp: '087824140294',
    alamat: 'Jl. Damai No.7',
    kota: 'Bandung',
    provinsi: 'Jawa Barat',
    avatar: ProfilePict
}