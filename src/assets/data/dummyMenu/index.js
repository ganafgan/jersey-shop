import Icon from 'react-native-vector-icons/Ionicons'
import { Colors } from '../../../utils/Colors';
import React from 'react'

export const dummyMenu = [
    {
      id: 1,
      nama: 'Edit Profile',
      gambar: <Icon name='create' size={25} color={Colors.primary}/>,
      halaman: 'EditProfile'
    },
    {
      id: 2,
      nama: 'Change Password',
      gambar: <Icon name='lock-closed' size={25} color={Colors.primary}/>,
      halaman: 'ChangePassword'
    },
    {
      id: 3,
      nama: 'History Pemesanan',
      gambar: <Icon name='list' size={25} color={Colors.primary}/>,
      halaman: 'History'
    },
    {
      id: 4,
      nama: 'Sign Out',
      gambar: <Icon name='log-out' size={25} color={Colors.primary}/>,
      halaman: 'Login'
    },
  ];