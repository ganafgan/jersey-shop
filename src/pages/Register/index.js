import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, KeyboardAvoidingView,Platform, Keyboard, TouchableWithoutFeedback, ScrollView, Alert } from 'react-native'
import { Colors } from '../../utils/Colors'
import { Fonts } from '../../utils/Fonts'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { Button, Gap, Input, Options } from '../../components'
import { RFValue } from 'react-native-responsive-fontsize'
import { heightMobileUI } from '../../utils/Constant'
import { useDispatch, useSelector } from 'react-redux'
import { getKotaList, getProvinsiList } from '../../redux/actions/ongkirAction'
import { showError, showSuccess } from '../../utils/showMessage'
import { registerUser } from '../../redux/actions/authAction'

const Register = (props) => {

    const disptach = useDispatch()
    const { provinsiResult, kotaResult } = useSelector(state => state.ongkirReducer)

    const [data, setData] = useState({
        nama:'',
        email: '',
        nohp: '',
        alamat: '',
    })

    const [password, setPassword] = useState('')
    const [provinsi, setProvinsi] = useState('')
    const [kota, setKota] = useState('')

    useEffect(() => {
        disptach(getProvinsiList())
    }, [])

    const handleChange = (name, value) => {
        setData({
            ...data,
            [name]: value
        })
    }

    const changeProvinsi = (provinsi) => {
        setProvinsi(provinsi)
        setKota('')
        disptach(getKotaList(provinsi))
    }

    const handleSubmit = () => {

        const dataProfile = {
            ...data,
            provinsi: provinsi,
            kota: kota,
            status: 'user'
        }
       
        if(data.nama && data.email && data.nohp && password && data.alamat && provinsi && kota){
            disptach(registerUser(dataProfile, password, props.navigation))
        } else {
            showError('Mohon lengkapi data')
        }
    }

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            style={styles.container}
            >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <View>
                            <Text style={styles.title}>Jersi</Text>
                            <Text style={styles.subtitle}>Pedia</Text>
                        </View>
                            <Input
                                label='Nama'
                                fontSize={RFValue(18, heightMobileUI)}
                                value={data.nama}
                                onChangeText={(value) => handleChange('nama', value)}
                            />
                            <Gap height={responsiveHeight(20)}/>
                            <Input
                                label='Email'
                                fontSize={RFValue(18, heightMobileUI)}
                                value={data.email}
                                onChangeText={(value) => handleChange('email', value)}
                            />
                            <Gap height={responsiveHeight(20)}/>
                            <Input
                                label='No Handphone'
                                fontSize={RFValue(18, heightMobileUI)}
                                keyboardType={'number-pad'}
                                value={data.nohp}
                                onChangeText={(value) => handleChange('nohp', value)}
                            />
                            <Gap height={responsiveHeight(20)}/>
                            <Input
                                label='Password'
                                fontSize={RFValue(18, heightMobileUI)}
                                secureTextEntry
                                value={password}
                                onChangeText={(value) => setPassword(value)}
                            />
                            <Gap height={responsiveHeight(20)}/>
                             <Input
                                textarea
                                label='Alamat'
                                fontSize={RFValue(18, heightMobileUI)}
                                value={data.alamat}
                                onChangeText={(value) => handleChange('alamat', value)}
                            />
                            <Gap height={responsiveHeight(20)}/>
                            <Options
                                label='Provinsi'
                                datas={provinsiResult}
                                fontSize={RFValue(18, heightMobileUI)}
                                selectedValue={provinsi}
                                onValueChange={(provinsi) => changeProvinsi(provinsi)}
                            />
                            <Gap height={responsiveHeight(20)}/>
                            <Options
                                label='Kota/Kabupaten'
                                datas={kotaResult}
                                fontSize={RFValue(18, heightMobileUI)}
                                selectedValue={kota}
                                onValueChange={(kota) => setKota(kota)}
                            />
                            <Gap height={responsiveHeight(100)}/>
                    </View>
                    <Button
                        type='text'
                        title='Continue'
                        padding={10}
                        onPress={handleSubmit}
                    />
                </ScrollView>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
        
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: Colors.white,
        paddingHorizontal: responsiveWidth(30),
        paddingVertical: responsiveWidth(30),
    },
    title: {
        fontFamily: Fonts.primary[600],
        fontSize: RFValue(40, heightMobileUI),
        color: Colors.primary
    },
    subtitle: {
        fontFamily: Fonts.primary[400],
        fontSize: RFValue(40, heightMobileUI),
        color: Colors.primary,
        marginBottom: responsiveHeight(30)
    }
})
