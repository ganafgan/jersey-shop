import React, {useEffect, useState} from 'react'
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { RFValue } from 'react-native-responsive-fontsize'
import { useDispatch, useSelector } from 'react-redux'
import { dummyJerseys, dummyLiga } from '../../assets/data'
import { BannerSlider, Button, Gap, Header, ListJersey, ListLiga } from '../../components'
import { getListJersey } from '../../redux/actions/jerseyAction'
import { getListLiga } from '../../redux/actions/ligaAction'
import { Colors } from '../../utils/Colors'
import { heightMobileUI } from '../../utils/Constant'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { Fonts } from '../../utils/Fonts'

const Jersey = (props) => {

    const dispatch = useDispatch();
    const { navigation } = props
    const { idLiga, namaLiga, keyword } = useSelector(state => state.jerseyReducer)
    
    useEffect(() => {
        if (idLiga) {
            dispatch(getListJersey(idLiga, keyword))
        } 

        if (keyword) {
            dispatch(getListJersey(idLiga, keyword))
        } 

        const unsubscribe = navigation.addListener('focus', () => {
            // do something
            dispatch(getListLiga())
            dispatch(getListJersey(idLiga))
        });

        return unsubscribe
    }, [props.navigation, idLiga, keyword])
   
    return (
        <View style={styles.container}>
            <Header 
                navigation={navigation} 
                page='Jersey'
                dispatch={dispatch}
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.ligas}>
                    <ListLiga 
                        navigation={navigation}
                        dispatch={dispatch}
                    />
                </View>
                <Gap height={responsiveHeight(0)}/>
                <View style={styles.ligas}>
                    {
                        keyword ?
                        <Text style={{fontFamily: Fonts.primary[400], marginBottom: responsiveHeight(10)}}>
                            Hasil pencarian <Text style={styles.title}>{keyword}</Text>
                        </Text> : null   
                    }
                    {
                        !keyword ?
                        <Text style={styles.title}>
                            Pilih Jersey {namaLiga ? namaLiga : 'yang anda inginkan'}
                        </Text> : null
                    }
                    <ListJersey
                        navigation={navigation}
                    />
                </View>
                <Gap height={responsiveHeight(125)}/>
            </ScrollView>
        </View>
    )
}

export default Jersey

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    ligas: {
        marginHorizontal: responsiveWidth(30),
        marginTop: responsiveHeight(30)
    },
    title: {
        fontFamily: Fonts.primary[500],
        fontSize: RFValue(18, heightMobileUI),
        marginBottom: responsiveHeight(10)
    },
    wrapperLink: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    link: {
        fontSize: 14,
        fontFamily: Fonts.primary[500],
        color: Colors.primary,
        marginBottom: responsiveHeight(10)
    },
    label: {
        fontSize: 16,
        fontFamily: Fonts.primary[500],
        color: Colors.black,
        marginBottom: responsiveHeight(10)
    },
})
