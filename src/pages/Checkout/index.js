import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Colors } from '../../utils/Colors'
import { Fonts } from '../../utils/Fonts'
import Icon from 'react-native-vector-icons/Ionicons'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { RFValue } from 'react-native-responsive-fontsize'
import { heightMobileUI, numberWithCommas } from '../../utils/Constant'
import { Button, CardAddress, Gap, Options } from '../../components/atoms'
import { dummyPesanans, dummyProfile } from '../../assets/data'

const Checkout = (props) => {

    const [carts, setCarts] = useState(dummyPesanans[0])
    const [address, setAddress] = useState(dummyProfile)
    const [expedition, setExpedition] = useState([])

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            title: 'Checkout',
            headerStyle: {
                backgroundColor: Colors.white,
            
            },
            headerTitleStyle: {
                fontFamily: Fonts.primary[200],
                fontSize: 18
            },
            headerLeft: () => (
                <View style={{flexDirection: 'row', marginRight: responsiveWidth(20)}}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}>
                        <Icon name='chevron-back-outline' size={25} color={Colors.black} />
                    </TouchableOpacity>
                </View>
            )
        })
    }, [props.navigation])

    return (
        <View style={styles.container}> 
            <View>

                <View>
                    <Text style={styles.title}>Apakah benar alamat ini ?</Text>
                        <CardAddress
                            address={address}
                            onPress={() => props.navigation.navigate('Profile')}
                        />
                </View>
                <View style={styles.wrapperPrice}>
                    <Text style={styles.total}>Total Harga</Text>
                    <Text style={styles.price}>Rp {numberWithCommas(carts.totalHarga)}</Text>
                </View>
                <Gap height={responsiveHeight(30)}/>
                
                    <Options
                        label='Pilih Ekspedisi :'
                        datas={expedition}
                        fontSize={RFValue(18, heightMobileUI)}
                    />
                <Gap height={responsiveHeight(30)}/>
                <View>
                    <Text style={styles.title}>Biaya Ongkir</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={styles.subtitle}>Berat : {carts.berat} Kg</Text>
                        <Text style={styles.value}>Rp {numberWithCommas(150000)}</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={styles.subtitle}>Estimasi</Text>
                        <Text style={styles.value}>2 - 3 Hari</Text>
                    </View>
                </View>
            </View>
            <View style={styles.footer}>
                <View style={styles.wrapperTotal}>
                    <Text style={styles.total}>Total Harga</Text>
                    <Text style={styles.price}>Rp {numberWithCommas(550000)}</Text>
                </View>
                <Button
                title='Bayar'
                type='textIcon'
                icon={<Icon name='arrow-forward' size={25} color={Colors.white}/>}
                color={Colors.primary}
                padding={responsiveHeight(15)}
                onPress={() => props.navigation.navigate('Checkout')}
                />
           </View>
        </View>
    )
}

export default Checkout

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        paddingVertical: responsiveHeight(30),
        paddingHorizontal: responsiveWidth(30),
        justifyContent: 'space-between'
    },
    title: {
        fontFamily: Fonts.primary[500],
        fontSize: RFValue(20, heightMobileUI)
    },
    wrapperPrice: {
        marginTop: responsiveHeight(30),
        justifyContent: 'space-between', 
        flexDirection: 'row'
    },
    total: {
        fontSize: RFValue(20, heightMobileUI),
        fontFamily: Fonts.primary[500]
    },
    price: {
        fontSize: RFValue(20, heightMobileUI),
        fontFamily: Fonts.primary[500]
    },
    subtitle: {
        fontSize: RFValue(18, heightMobileUI),
        fontFamily: Fonts.primary[500]
    },
    value: {
        fontSize: RFValue(18, heightMobileUI),
        fontFamily: Fonts.primary[500],
        color: Colors.grey1
    },
    wrapperTotal: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: responsiveHeight(30),
        marginTop: responsiveHeight(30)
    },
})
