import Splash from './Splash'
import Home from './Home'
import Jersey from './Jersey'
import Profile from './Profile'
import JerseyDetail from './JerseyDetail'
import Cart from './Cart'
import Checkout from './Checkout'
import EditProfile from './EditProfile'
import ChangePassword from './ChangePassword'
import History from './History'
import Login from './Login'
import Register from './Register'

export {
    Splash,
    Home,
    Jersey,
    Profile,
    JerseyDetail,
    Cart,
    Checkout,
    EditProfile,
    ChangePassword,
    History,
    Login,
    Register, 
}