import React, { useEffect, useState } from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Logo } from '../../assets'
import { Colors } from '../../utils/Colors'
import { Fonts } from '../../utils/Fonts'
import * as Animatable from 'react-native-animatable';
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { Button, Gap, Input } from '../../components/atoms'
import { Link } from '@react-navigation/native'
import { RFValue } from 'react-native-responsive-fontsize'
import { heightMobileUI } from '../../utils/Constant'
import { useDispatch, useSelector } from 'react-redux'
import { loginUser } from '../../redux/actions/authAction'
import { showError } from '../../utils/showMessage'
import Loading from '../../components/moleculs/Loading'
import { setLoading } from '../../redux/actions/loadingAction'

const { height, width } = Dimensions.get('window')

const Login = (props) => {

    const disptach = useDispatch()

    const [data, setData] = useState({
        email: '',
        password: ''
    })
   
    const handleLogin = () => {
        if(data.email && data.password){
            disptach(loginUser(data.email, data.password, props.navigation))
        } else {
            showError('Email dan Password tidak boleh kosong')
        }
    }

    const handleChange = (name, value) => {
        setData({
            ...data,
            [name]: value
        })
    }

    return (
        <View style={styles.container}>
            <View style={styles.logo}>
                <Logo height={height * 0.1} width={width * 0.3}/>
            </View>
            <View style={styles.input}>
                <Input
                    label='Email'
                    fontSize={RFValue(18, heightMobileUI)}
                    value={data.email}
                    onChangeText={(value) => handleChange('email', value)}
                />
                <Gap height={responsiveHeight(20)}/>
                <Input
                    label='Password'
                    secureTextEntry
                    fontSize={RFValue(18, heightMobileUI)}
                    value={data.password}
                    onChangeText={(value) => handleChange('password', value)}
                />
                <Gap height={responsiveHeight(70)}/>
                <Button
                    type='text'
                    title='Login'
                    padding={10}
                    onPress={handleLogin}
                />
            </View>
            <View style={styles.link}>
                <Text style={styles.left}>Belum punya akun ? </Text>
                <TouchableOpacity onPress={() => props.navigation.navigate('Register')}>
                    <Text style={styles.right}>Klik disini</Text>
                </TouchableOpacity>
                    
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        paddingTop: responsiveHeight(110)
    },
    logo: {
       alignItems: 'center'
    },
    input: {
        marginTop: responsiveHeight(50),
        paddingHorizontal: responsiveWidth(30)
    },
    link: {
        marginTop: responsiveHeight(50),
        justifyContent: 'center',
        flexDirection: 'row'
    },
    left: {
        fontSize: RFValue(18, heightMobileUI),
        fontFamily: Fonts.primary[500]
    },
    right: {
        fontSize: RFValue(18, heightMobileUI),
        fontFamily: Fonts.primary[500],
        color: Colors.primary
    }
})
