import React, { useEffect } from 'react'
import { Dimensions, StyleSheet, Text, View } from 'react-native'
import { Logo } from '../../assets'
import { Colors } from '../../utils/Colors'
import { Fonts } from '../../utils/Fonts'
import * as Animatable from 'react-native-animatable';

const { height, width } = Dimensions.get('window')

const Splash = (props) => {

    useEffect(() => {
        setTimeout(() => {
            props.navigation.replace('MainApp')
        }, 2000)
    }, [])
    return (
        <View style={styles.container}>
            <Animatable.View animation='fadeInDown' direction='normal'>
                <Logo height={height * 0.1} width={width * 0.3}/>
            </Animatable.View>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.white
    }
})
