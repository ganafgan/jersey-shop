import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native'
import { Colors } from '../../utils/Colors'
import { Fonts } from '../../utils/Fonts'
import Icon from 'react-native-vector-icons/Ionicons'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { dummyProfile } from '../../assets/data/dummyProfile'
import { Button, Gap, Input, Options } from '../../components/atoms'
import { RFValue } from 'react-native-responsive-fontsize'
import { heightMobileUI } from '../../utils/Constant'
import { getData } from '../../utils/localStorage'
import { useDispatch, useSelector } from 'react-redux'
import { getKotaList, getProvinsiList } from '../../redux/actions/ongkirAction'
import { updateProfile } from '../../redux/actions/profileAction'
import { AlertModal } from '../../components/moleculs'

const EditProfile = (props) => {

    const dispatch = useDispatch()
    const { provinsiResult, kotaResult } = useSelector(state => state.ongkirReducer)
    
    const [data, setData] = useState({
        uid: '',
        nama: '',
        email: '',
        nohp: '',
        alamat: '',
        provinsi: ''
    })

    const [provinsi, setProvinsi] = useState('')
    const [kota, setKota] = useState('')
   
    useEffect(() => {
        getUserData()
        dispatch(getProvinsiList())
    }, [])

    const getUserData = async () => {
        getData('userData').then((res) => {
            setData({
                ...data,
                uid: res.uid,
                nama: res.nama,
                email: res.email,
                nohp: res.nohp,
                alamat: res.alamat,
            })
            dispatch(getKotaList(res.provinsi))
            setProvinsi(res.provinsi)
            setKota(res.kota)
        })
    }

    const changeProvinsi = (provinsi) => {
        setProvinsi(provinsi)
        setKota('')
        dispatch(getKotaList(provinsi))
    }

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            title: 'Edit Profile',
            headerStyle: {
                backgroundColor: Colors.white,
            },
            headerTitleStyle: {
                fontFamily: Fonts.primary[200],
                fontSize: 18
            },
            headerLeft: () => (
                <View style={{flexDirection: 'row', marginRight: responsiveWidth(20)}}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}>
                        <Icon name='chevron-back-outline' size={25} color={Colors.black} />
                    </TouchableOpacity>
                </View>
            )
        })
    }, [props.navigation])

    const handleChange = (name, value) => {
        setData({
            ...data,
            [name]: value
        })
    }

    const handleUpdate = () => {
        const dataUpdate = {
            ...data,
            provinsi,
            kota
        }
        dispatch(updateProfile(dataUpdate, data.uid, props.navigation))
    }

    return (
        <View style={styles.container}>
            <View>
                <Input
                    label='Nama'
                    fontSize={RFValue(18, heightMobileUI)}
                    value={data.nama}
                    onChangeText={(value) => handleChange('nama', value) }
                />
                <Gap height={responsiveHeight(20)}/>
                <Input
                    label='Email'
                    fontSize={RFValue(18, heightMobileUI)}
                    value={data.email}
                    onChangeText={(value) => handleChange('email', value) }
                    disabled
                />
                <Gap height={responsiveHeight(20)}/>
                <Input
                    label='No. Handphone'
                    fontSize={RFValue(18, heightMobileUI)}
                    value={data.nohp}
                    onChangeText={(value) => handleChange('nohp', value) }
                    keyboardType={'number-pad'}
                />
                <Gap height={responsiveHeight(20)}/>
                <Input
                    label='Alamat'
                    textarea
                    fontSize={RFValue(18, heightMobileUI)}
                    value={data.alamat}
                    onChangeText={(value) => handleChange('alamat', value) }
                />
                <Gap height={responsiveHeight(20)}/>
                <Options
                    label='Provinsi'
                    datas={provinsiResult && provinsiResult}
                    fontSize={RFValue(18, heightMobileUI)}
                    selectedValue={provinsi}
                    onValueChange={(provinsi) => changeProvinsi(provinsi)}
                />
                <Gap height={responsiveHeight(20)}/>
                <Options
                    label='Kota/Kabupaten'
                    datas={kotaResult && kotaResult}
                    fontSize={RFValue(18, heightMobileUI)}
                    selectedValue={kota}
                    onValueChange={(kota) => setKota(kota)}
                />
            </View>
            <View>
                <Button
                    title='Submit'
                    type='textIcon'
                    icon={<Icon name='arrow-forward' size={25} color={Colors.white}/>}
                    color={Colors.primary}
                    padding={responsiveHeight(15)}
                    onPress={handleUpdate}
                    
                />
            </View>
        </View>
    )
}

export default EditProfile

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: Colors.white,
        paddingHorizontal: responsiveWidth(30),
        paddingVertical: responsiveHeight(30),
        justifyContent: 'space-between'
    }
})
