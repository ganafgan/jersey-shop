import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Colors } from '../../utils/Colors'
import { Fonts } from '../../utils/Fonts'
import Icon from 'react-native-vector-icons/Ionicons'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { dummyPesanans } from '../../assets/data'
import { ListHistory } from '../../components'

const History = (props) => {

    const [pesanan, setPesanan] = useState(dummyPesanans)

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            title: 'History',
            headerStyle: {
                backgroundColor: Colors.white,
            
            },
            headerTitleStyle: {
                fontFamily: Fonts.primary[200],
                fontSize: 18
            },
            headerLeft: () => (
                <View style={{flexDirection: 'row', marginRight: responsiveWidth(20)}}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}>
                        <Icon name='chevron-back-outline' size={25} color={Colors.black} />
                    </TouchableOpacity>
                </View>
            )
        })
    }, [props.navigation])

    return (
        <View style={styles.container}>
           <ListHistory
                pesanans={pesanan}
           />
        </View>
    )
}

export default History

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    }
})
