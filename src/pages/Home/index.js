import React, {useEffect, useState} from 'react'
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { dummyJerseys, dummyLiga } from '../../assets/data'
import { BannerSlider, Button, Gap, Header, ListJersey, ListLiga } from '../../components'
import { Colors } from '../../utils/Colors'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { Fonts } from '../../utils/Fonts'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { API_KEY, heightMobileUI } from '../../utils/Constant'
import { useDispatch, useSelector } from 'react-redux';
import { getUser } from '../../redux/actions/userAction'
import axios from 'axios'
import { getListLiga } from '../../redux/actions/ligaAction'
import { getFavoriteJersey, getListJersey } from '../../redux/actions/jerseyAction'

const Home = (props) => {

    const dispatch = useDispatch();
    const { navigation } = props

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            // do something
            dispatch(getListLiga())
            dispatch(getFavoriteJersey())
        });

        return unsubscribe
    }, [props.navigation])
    
    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Header 
                    navigation={navigation} 
                    page='Home'
                    dispatch={dispatch}
                />
                <BannerSlider/>
                <View style={styles.ligas}>
                    <View style={styles.wrapperLink}>
                        <Text style={styles.label}>Pilih Liga</Text>
                        <TouchableOpacity>
                            <Text style={styles.link}>View All</Text>
                        </TouchableOpacity>
                    </View>
                    <ListLiga 
                        navigation={navigation}
                        dispatch={dispatch}
                    />
                </View>
                <View style={styles.ligas}>
                    <Text style={styles.label}>Pilih Jersey</Text>
                    <ListJersey
                        navigation={navigation}
                    />
                </View>
                <Gap height={responsiveHeight(125)}/>
            </ScrollView>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    ligas: {
        marginHorizontal: responsiveWidth(30),
        marginTop: responsiveHeight(30)
        
    },
    wrapperLink: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    link: {
        fontSize: RFValue(14, heightMobileUI),
        fontFamily: Fonts.primary[500],
        color: Colors.primary,
        marginBottom: responsiveHeight(10)
    },
    label: {
        fontSize: RFValue(18, heightMobileUI),
        fontFamily: Fonts.primary[500],
        color: Colors.black,
        marginBottom: responsiveHeight(10)
    },
})
