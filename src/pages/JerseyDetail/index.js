import React, { useEffect, useState } from 'react'
import { KeyboardAvoidingView, ScrollView, StyleSheet, Text, View } from 'react-native'
import { Button, CardLiga, Gap, Input, JerseySlider, Options } from '../../components'
import { Colors } from '../../utils/Colors'
import Icon from 'react-native-vector-icons/Ionicons'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { RFValue } from 'react-native-responsive-fontsize'
import { heightMobileUI, numberWithCommas } from '../../utils/Constant'
import { Fonts } from '../../utils/Fonts'
import { useDispatch, useSelector } from 'react-redux'
import { getDetailLiga } from '../../redux/actions/ligaAction'
import { getData } from '../../utils/localStorage'
import { showError } from '../../utils/showMessage'

const JerseyDetail = (props) => {

    const {navigation} = props
    
    const { jersey } = props.route.params
    const dispatch = useDispatch()
    const { listDetailLigaResult } = useSelector(state => state.ligaReducer)

    const [total, setTotal] = useState('')
    const [size, setSize] = useState('')
    const [description, setDescription] = useState('')
    const [uid, setUid] = useState('')
   
    useEffect(() => {
        dispatch(getDetailLiga(jersey.liga))
    }, [])

    const handleCart = () => {

        getData('userData')
        .then((res) => {
            if(res) {
                //set uid dari lcoal storage ke state
                setUid(res.uid)

                //validasi form
                if (total && size && description) {
                    //action keranjang

                } else {
                    showError('Form harus diisi lengkap')
                }

            } else {
                showError('Silahkan login terlebih dahulu')
                navigation.replace('Login')
            }
        })

    }

    return (
        <View style={styles.page}>
            <View>
                <View style={styles.button}>
                    <Button
                        icon={<Icon 
                                name='chevron-back' 
                                size={25} 
                                color={Colors.black}
                            />}
                        color={Colors.white}
                        padding={8}
                        onPress={() => navigation.goBack()}
                    />
                </View>
                <JerseySlider
                   images={jersey.gambar}
                />
            </View>
            <View style={styles.container}>
                <View>
                    <View style={styles.liga}>
                        <CardLiga 
                            liga={listDetailLigaResult && listDetailLigaResult} 
                            navigation={navigation}
                            id={jersey && jersey.liga}
                            dispatch={dispatch}
                        />
                    </View>
                    <Text style={styles.name}>{jersey.nama}</Text>
                    <Text style={styles.price}>Rp {numberWithCommas(jersey.harga)}</Text>
                    <View style={styles.wrapperJenis}>
                        <Text style={styles.jenis}>Jenis : {jersey.jenis}</Text>
                        <Text style={styles.berat}>Berat: {jersey.berat} kg</Text>
                    </View>
                    <View style={styles.wrapperPicker}>
                        <Input
                            label='Jumlah'
                            width={responsiveWidth(166)}
                            height={responsiveHeight(46)}
                            fontSize={13}
                            keyboardType='number-pad'
                            value={total}
                            onChangeText={(value) => setTotal(value)}
                        />
                    <Options
                           label='Size'
                            width={responsiveWidth(160)}
                            height={responsiveHeight(46)}
                            fontSize={13}
                            datas={jersey && jersey.ukuran}
                            selectedValue={size}
                            onValueChange={(value) => setSize(value)}
                            
                    />
                    </View>
                    <Gap height={responsiveHeight(20)}/>
                    <Input
                        label='Keterangan'
                        textarea
                        fontSize={13}
                        placeHolder='Isi keterangn jersey'
                        value={description}
                        onChangeText={(value) => setDescription(value)}
                    />
                </View>
                <View>
                    <Button
                        title='Masuk Keranjang'
                        type='textIcon'
                        icon={<Icon name='cart' size={25} color={Colors.white}/>}
                        color={Colors.primary}
                        padding={responsiveHeight(15)}
                        fontSize={18}
                        onPress={handleCart}
                    />

                </View>
            </View>
        </View>
    )
}

export default JerseyDetail

const styles = StyleSheet.create({
    page: {
        flex: 1,
        justifyContent: 'space-between',
    },
    container: {
        backgroundColor: Colors.white,
        height: '60%',
        justifyContent: 'space-between',
        paddingHorizontal: responsiveWidth(30),
        paddingVertical: responsiveHeight(30)
    },
    liga: {
        alignItems: 'flex-end',
        marginTop: -responsiveHeight(70)
    },
    name: {
        fontSize: RFValue(20, heightMobileUI),
        fontFamily: Fonts.primary[500],
        
    },
    price: {
        fontSize: RFValue(20, heightMobileUI),
        fontFamily: Fonts.primary[500],
        marginBottom: responsiveHeight(20)
    },
    button: {
        position: 'absolute',
        marginLeft: responsiveWidth(30),
        marginTop: responsiveHeight(30),
        zIndex: 1
    },
    wrapperJenis: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    jenis: {
        fontSize: RFValue(14, heightMobileUI),
        fontFamily: Fonts.primary[400],
    },
    berat: {
        fontSize: RFValue(14, heightMobileUI),
        fontFamily: Fonts.primary[400],
    },
    wrapperPicker: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: responsiveHeight(20)
    }
})
