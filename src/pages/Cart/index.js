import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity} from 'react-native'
import { Colors } from '../../utils/Colors'
import { Fonts } from '../../utils/Fonts'
import Icon from 'react-native-vector-icons/Ionicons'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import ListCart from '../../components/moleculs/ListCart'
import { dummyPesanans } from '../../assets/data'
import { Button } from '../../components/atoms'
import { heightMobileUI, numberWithCommas } from '../../utils/Constant'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const Cart = (props) => {

    const [carts, setCarts] = useState(dummyPesanans[0])

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            title: 'Cart',
            headerStyle: {
                backgroundColor: Colors.white,
            
            },
            headerTitleStyle: {
                fontFamily: Fonts.primary[200],
                fontSize: 18
            },
            headerLeft: () => (
                <View style={{flexDirection: 'row', marginRight: responsiveWidth(20)}}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}>
                        <Icon name='chevron-back-outline' size={25} color={Colors.black} />
                    </TouchableOpacity>
                </View>
            )
        })
    }, [props.navigation])

    return (
        <View style={styles.container}>
            <ListCart
                carts={carts.pesanans}
            />
           <View style={styles.footer}>
                <View style={styles.wrapperTotal}>
                    <Text style={styles.total}>Total Harga</Text>
                    <Text style={styles.price}>Rp {numberWithCommas(carts.totalHarga)}</Text>
                </View>
                <Button
                   title='Check Out'
                   type='textIcon'
                   icon={<Icon name='arrow-forward' size={25} color={Colors.white}/>}
                   color={Colors.primary}
                   padding={responsiveHeight(15)}
                   onPress={() => props.navigation.navigate('Checkout')}
                />
           </View>
        </View>
    )
}

export default Cart

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        paddingVertical: responsiveHeight(10)
    },
    footer: {
        marginHorizontal: responsiveWidth(30),
        marginBottom: responsiveHeight(20)
    },
    wrapperTotal: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: responsiveHeight(30)
    },
    total: {
        fontSize: RFValue(20, heightMobileUI),
        fontFamily: Fonts.primary[500]
    },
    price: {
        fontSize: RFValue(20, heightMobileUI),
        fontFamily: Fonts.primary[500]
    }
})
