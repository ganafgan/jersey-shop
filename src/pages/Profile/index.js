import React, { useEffect } from 'react'
import { useState } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { Colors } from '../../utils/Colors'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { Fonts } from '../../utils/Fonts'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { dummyAvatar_url, heightMobileUI } from '../../utils/Constant'
import { dummyMenu } from '../../assets/data/dummyMenu'
import { Gap } from '../../components/atoms'
import { clearStorage, getData } from '../../utils/localStorage'
import Modal from "react-native-modal";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { showError, showSuccess } from '../../utils/showMessage'
import storage from '@react-native-firebase/storage';
import moment from 'moment'
import { useDispatch, useSelector } from 'react-redux'
import { updateAvatar } from '../../redux/actions/profileAction'
import { ConfirmModal, CardMenu } from '../../components'
import auth from '@react-native-firebase/auth';


const Profile = (props) => {

    const { navigation } = props
    const dispatch = useDispatch()

    const [profile, setProfile] = useState({
        nama: '',
        nohp:'',
        uid:'',
        photo: ''
    })
    const [menus, setMenus] = useState(dummyMenu)
    const [date, setDate] = useState(moment(new Date()).format('DDMMYY'))
    const [avatarModal, setAvatarModal ] = useState(false)
    const [signoutModal, setSignoutModal] = useState(false)
    
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            // do something
            getUserData()
        });

        return unsubscribe
    }, [props.navigation])
    
    const getUserData = async () => {
        try {
            const res = await getData('userData')
            if(res) {
                setProfile({
                    nama: res.nama,
                    nohp: res.nohp,
                    uid: res.uid,
                    photo: res.photo_profile && res.photo_profile
                })
            } else {
                props.navigation.replace('Login')
            }
        } catch (error) {
            
        }
    }

    const getPictCamera = () => {

        let options = {
            quality: 1,
            saveToPhotos: true,
            maxWidth: 700,
            maxHeight: 700
        }

        launchCamera( options, (response) => {

            if(response.didCancel || response.errorCode || response.errorMessage){
               
            } else {
                const img_name = `profile_${profile.nama}_${date}`
                const uri  = response.assets[0].uri
                setProfile({...profile, photo: uri})
                uploadFile(img_name, uri)
                setAvatarModal(!avatarModal)
                showSuccess('Change photo success')
            } 
            
        })
    }

    const getPictGallery = () => {

        let options = {
            quality: 1,
            saveToPhotos: true,
            maxWidth: 700,
            maxHeight: 700
        }

        launchImageLibrary(options, (response) => {
            
            if(response.didCancel || response.errorCode || response.errorMessage){
               
            } else {
                const img_name = `profile_${profile.nama}_${date}`
                const uri  = response.assets[0].uri
                setProfile({...profile, photo: uri})
                uploadFile(img_name, uri)
                setAvatarModal(!avatarModal)
                showSuccess('Change photo success')
            } 
        })
    }

    const uploadFile = async (img_name, uri) => {
        let pathToFile = `${uri}`
        await storage().ref(img_name).putFile(pathToFile)
        let url = await storage().ref(img_name).getDownloadURL()
        await dispatch(updateAvatar(url, profile.uid))
    }

    const handleLogout = () => {
        auth().signOut()
        .then((res) => {
            clearStorage()
            props.navigation.replace('Login')
        })
        .catch((error) => {
            showError(error)
        })
    }
    
    return (
        <View style={styles.container}>
            
            <View style={styles.profile}>
                <View style={styles.borderAvatar}>
                    {
                        !profile.photo || profile.photo == ''
                        ?
                        <Image
                            source={{uri: dummyAvatar_url}}
                            style={styles.avatar}
                        />
                        :
                        <Image
                            source={{uri: profile.photo}}
                            style={styles.avatar}
                        />
                    }
                    <View style={styles.iconPict}>
                        <TouchableOpacity onPress={() => setAvatarModal(!avatarModal)}>
                                {
                                    !profile.photo ?
                                    <Icon name='camera-outline' size={24} color={Colors.white} />:
                                    <Icon name='close-outline' size={24} color={Colors.white} />
                                }
                        </TouchableOpacity>
                    </View>
                </View>
                <Text style={styles.nama}>{profile && profile.nama}</Text>
            </View>
            <Gap height={responsiveHeight(100)}/>
            <View>
                <CardMenu
                    nama='Edit Profile'
                    gambar={<Icon name='create' size={25} color={Colors.primary}/>}
                    onPress={() => props.navigation.navigate('EditProfile')}
                />
               <CardMenu
                    nama='Change Password'
                    gambar={<Icon name='lock-closed' size={25} color={Colors.primary}/>}
                    onPress={() => props.navigation.navigate('ChangePassword')}
                />
                <CardMenu
                    nama='History Pemesanan'
                    gambar={<Icon name='list' size={25} color={Colors.primary}/>}
                    onPress={() => props.navigation.navigate('History')}
                />
                <CardMenu
                    nama='Logout'
                    gambar={<Icon name='log-out' size={25} color={Colors.primary}/>}
                    onPress={() => setSignoutModal(!signoutModal)}
                />
            </View>
                                
            <Modal
                style={{margin: 0, flex: 1}}
                isVisible={avatarModal}
                onBackButtonPress={() =>setAvatarModal(!avatarModal)}
                onBackdropPress={() =>setAvatarModal(!avatarModal)}
                useNativeDriver={true}
                animationIn='slideInUp'
                backdropColor={Colors.backdrop}
            >
                <View style={styles.wrapperModal}>
                    <View style={styles.modalView}>
                        <View style={styles.wrapperIcon}>
                                <View style={styles.icon}>
                                    <TouchableOpacity onPress={getPictCamera}>
                                            <Icon name='camera-outline' size={30} color={Colors.white}/>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.titleIcon}>Camera</Text>
                        </View>
                        <Gap width={responsiveWidth(75)}/>
                        <View style={styles.wrapperIcon}>
                            <View style={styles.icon}>
                                <TouchableOpacity onPress={getPictGallery}>
                                    <Icon name='image-outline' size={30} color={Colors.white}/>
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.titleIcon}>Gallery</Text>
                        </View>
                    </View>
                </View>
            </Modal>
            <ConfirmModal
                isVisible={signoutModal}
                onBackButtonPress={() => setSignoutModal(!signoutModal)}
                onBackdropPress={() => setSignoutModal(!signoutModal)}
                title='Confirmation'
                text='Are you sure want to logout ?'
                onCancel={() => setSignoutModal(!signoutModal)}
                onOk={handleLogout}
            />
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        paddingTop: responsiveHeight(30),
    },
    profile: {
        alignItems: 'center',
    },
    borderAvatar: {
        height: 130,
        width: 130,
        borderWidth:1,
        borderColor: '#e9e9e9',
        borderRadius: 130/2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        height: 110,
        width: 110,
        borderRadius: 110/2,
    },
    iconPict: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        height:40 ,
        width: 40,
        backgroundColor: Colors.primary,
        borderRadius: 40/2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nama: {
        marginTop: responsiveHeight(10),
        fontSize: RFValue(18, heightMobileUI),
        fontFamily: Fonts.primary[500]
    },
    wrapperModal: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    modalView: {
        backgroundColor: Colors.white,
        paddingVertical: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20
    },
    wrapperIcon: {
        alignItems: 'center'
    },
    icon: {
        height: 50,
        width: 50,
        borderRadius: 50/2,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleIcon: {
        marginTop: 5,
        color: Colors.primary,
        fontFamily: Fonts.primary[500],
        fontSize: RFValue(18, heightMobileUI)
    }
})
