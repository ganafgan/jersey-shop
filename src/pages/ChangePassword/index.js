import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Colors } from '../../utils/Colors'
import { Fonts } from '../../utils/Fonts'
import Icon from 'react-native-vector-icons/Ionicons'
import { responsiveHeight, responsiveWidth } from '../../utils/Dimension'
import { Button, Gap, Input } from '../../components/atoms'
import { RFValue } from 'react-native-responsive-fontsize'
import { heightMobileUI } from '../../utils/Constant'

const ChangePassword = (props) => {

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            title: 'Change Password',
            headerStyle: {
                backgroundColor: Colors.white,
            
            },
            headerTitleStyle: {
                fontFamily: Fonts.primary[200],
                fontSize: 18
            },
            headerLeft: () => (
                <View style={{flexDirection: 'row', marginRight: responsiveWidth(20)}}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}>
                        <Icon name='chevron-back-outline' size={25} color={Colors.black} />
                    </TouchableOpacity>
                </View>
            )
        })
    }, [props.navigation])

    return (
        <View style={styles.container}>
            <View>

                <Input
                    label='Old Password'
                    fontSize={RFValue(18, heightMobileUI)}
                    secureTextEntry
                    value={'111111'}
                />
                <Gap height={responsiveHeight(20)}/>
                <Input
                    label='New Password'
                    fontSize={RFValue(18, heightMobileUI)}
                    secureTextEntry
                    value={'111111'}
                />
                <Gap height={responsiveHeight(20)}/>
                <Input
                    label='Confirm New Password'
                    fontSize={RFValue(18, heightMobileUI)}
                    secureTextEntry
                    value={'111111'}
                />
                <Gap height={responsiveHeight(20)}/>
            </View>
            <View>
                <Button
                    title='Submit'
                    type='textIcon'
                    icon={<Icon name='arrow-forward' size={25} color={Colors.white}/>}
                    color={Colors.primary}
                    padding={responsiveHeight(15)}
                    onPress={() => props.navigation.navigate('Checkout')}
                />
            </View>
        </View>
    )
}

export default ChangePassword

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: Colors.white,
        paddingHorizontal: responsiveWidth(30),
        paddingVertical: responsiveHeight(30),
        justifyContent: 'space-between'
    }
})
