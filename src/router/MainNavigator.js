import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
    Cart,
    ChangePassword,
    Checkout,
    EditProfile,
    History,
    Home, 
    Jersey, 
    JerseyDetail, 
    Login, 
    Profile, 
    
    Register, 
    
    Splash 
} 
from '../pages';
import { BottomNavigator } from '../components';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomNavigator {...props}/>}>
            <Tab.Screen 
                name='Home'
                component={Home}
                options={{headerShown: false}}
            />
            <Tab.Screen 
                name='Jersey'
                component={Jersey}
                options={{headerShown: false}}
            />
            <Tab.Screen 
                name='Profile'
                component={Profile}
                options={{headerShown: false}}
            />
        </Tab.Navigator>
    )
}

const MainNavigator = () => {
    return (
        <Stack.Navigator >
            <Stack.Screen
                name='Splash'
                component={Splash}
                options={{headerShown: false}}
            />
            <Stack.Screen 
                name='MainApp'
                component={MainApp}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name='JerseyDetail'
                component={JerseyDetail}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name='Cart'
                component={Cart}
            />
            <Stack.Screen
                name='Checkout'
                component={Checkout}
            />
            <Stack.Screen 
                name='EditProfile'
                component={EditProfile}
            />
            <Stack.Screen 
                name='ChangePassword'
                component={ChangePassword}
            />
            <Stack.Screen 
                name='History'
                component={History}
            />
            <Stack.Screen 
                name='Login'
                component={Login}
                options={{headerShown: false}}
            />
            <Stack.Screen 
                name='Register'
                component={Register}
                options={{headerShown: false}}
            />
        </Stack.Navigator>
    )
}

export default MainNavigator

